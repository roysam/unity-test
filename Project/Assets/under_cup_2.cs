using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class under_cup_2 : MonoBehaviour
{
    public GameObject inner_cup;
    public GameObject beaker;
    public bool is_up;
    public float X_min, X_max, Z_min, Z_max;
    public GameObject water_level;
    private GameObject water;
    private bool on;
    // Start is called before the first frame update
    void Start()
    {
        is_up = false;
        water = water_level.transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        BoxCollider col = inner_cup.GetComponent<BoxCollider>();
        var trans = col.transform;
        var min = col.center - col.size * 0.5f;
        var max = col.center + col.size * 0.5f;
        var P000 = trans.TransformPoint(new Vector3(min.x, min.y, min.z));
        var P001 = trans.TransformPoint(new Vector3(max.x, max.y, max.z));
        X_min = (P000.x < P001.x ? P000.x : P001.x)-0.2f;
        X_max = P000.x < P001.x ? P001.x : P000.x+0.2f;
        Z_min = P000.z < P001.z ? P000.z : P001.z-0.2f;
        Z_max = P000.z < P001.z ? P001.z : P000.z+0.2f;
        if ((transform.position.x < X_max && transform.position.x > X_min) && (transform.position.z < Z_max && transform.position.z > Z_min))
        {
            is_up = true;
            //water.GetComponent<water_level_cup>().poured = is_up;
            var myoutline = inner_cup.GetComponent<Outline>();
            myoutline.enabled = true;
            on = true;
        }
        else
        {
            is_up = false;
            //water.GetComponent<water_level_cup>().poured = is_up;
            var myoutline = inner_cup.GetComponent<Outline>();
            if(on)
            {
             myoutline.enabled =false;
             on = false;
            }

        }
        if(is_up && beaker.GetComponent<tilt_beaker>().tilting)
        {
            water.GetComponent<water_level_cup>().poured = true;
        }
        
        //print(is_up);
    }
}
