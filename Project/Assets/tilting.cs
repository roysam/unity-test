using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tilting : MonoBehaviour
{
    private bool state;
    public GameObject tilt;
    public GameObject inner_water;
    public GameObject beaker_water;
    // Start is called before the first frame update
    void Start()
    {
        state = false;
        inner_water.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseOver()
    {
        if (gameObject.transform.position == tilt.transform.position)
        {
            if (Input.GetKeyDown(KeyCode.Q) && state == false)
            {
                Vector3 position = gameObject.GetComponent<Renderer>().bounds.center;
                gameObject.transform.RotateAround(position, new Vector3(1f, 0, 0), 90f);
                state = true;
                beaker_water.SetActive(false);
                inner_water.SetActive(true);
            }
        }
        if (Input.GetKeyDown(KeyCode.Z) && state == true)
        {
            Vector3 position = gameObject.GetComponent<Renderer>().bounds.center;
            gameObject.transform.RotateAround(position, new Vector3(1f, 0, 0), -90f);
            state = false;
        }
    }
}
