using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class metal_conduction : MonoBehaviour
{
    public float heatTransferCoefficient = 400;
    public float cylinderRadius = 0.03f;
    public float cylinderLength = 0.05f;
    public float liquidTemperature;
    public float specific_heat_capacity = 903f;
    public GameObject liquid_ambient;
    public GameObject liquid;
    public float currentTemperature;
    private float surfaceArea;
    public float heatLoss;
    private float mass;

    void Start()
    {
        currentTemperature = gameObject.GetComponent<own_temp>().temp;
        surfaceArea = 2 * Mathf.PI * cylinderRadius * cylinderLength + 2 * Mathf.PI * Mathf.Pow(cylinderRadius, 2);
        mass = gameObject.GetComponent<Rigidbody>().mass;
    }

    void Update()
    {
        currentTemperature = gameObject.GetComponent<own_temp>().temp;
        liquidTemperature = liquid.GetComponent<own_temp>().temp;
        if (liquid_ambient == gameObject.GetComponent<surround_medium>().surround)
        {
            heatLoss = heatTransferCoefficient * surfaceArea * (currentTemperature - liquidTemperature) * Time.deltaTime;
            currentTemperature = currentTemperature - ((heatLoss) / (mass * specific_heat_capacity));
            liquid.GetComponent<own_temp>().temp += ((heatLoss) / (liquid.GetComponent<Rigidbody>().mass * 4187));
            gameObject.GetComponent<own_temp>().temp = currentTemperature;
        }
    }
}
