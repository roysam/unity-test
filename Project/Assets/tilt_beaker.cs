using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tilt_beaker : MonoBehaviour
{
    private Vector3 rot;
    private float init_mass;
    public bool tilting;
    public float water_lost;
    public GameObject water;

    // Start is called before the first frame update
    void Start()
    {
        rot = gameObject.transform.eulerAngles;
        init_mass = gameObject.GetComponent<Rigidbody>().mass;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(1))
        {
            //waterpour.SetActive(false);
            gameObject.transform.eulerAngles = rot;
            tilting = false;

        }
    }
    private void OnMouseOver()
    {

        if (Input.GetMouseButtonDown(1))
        {
            //waterpour.SetActive(true);
            water_lost = gameObject.GetComponent<Rigidbody>().mass - init_mass;
            gameObject.transform.Rotate(new Vector3(90, 0, 0));
            gameObject.GetComponent<Rigidbody>().mass = init_mass;
            tilting = true;
            water.GetComponent<own_temp>().temp = 26f;


        }

    }
}
