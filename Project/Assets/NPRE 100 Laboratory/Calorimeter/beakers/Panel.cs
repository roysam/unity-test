using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel : MonoBehaviour
{
    public GameObject beakerpos;
    public GameObject beaker1pos;
    public GameObject beaker2pos;
    public GameObject curr_beaker;
    private Vector3 pos;
    // Start is called before the first frame update
    void Start()
    {
        pos = beakerpos.transform.localPosition - transform.localPosition;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(beakerpos.activeSelf == true)
        {
            transform.localPosition = beakerpos.transform.localPosition - pos;
        }
        if (beaker1pos.activeSelf == true)
        {
            transform.localPosition = beaker1pos.transform.localPosition - pos;
        }
        if (beaker2pos.activeSelf == true)
        {
            transform.localPosition = beaker2pos.transform.localPosition - pos;
        }


    }
}
