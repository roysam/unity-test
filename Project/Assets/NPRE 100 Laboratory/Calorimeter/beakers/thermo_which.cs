using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class thermo_which : MonoBehaviour
{
    public GameObject thermo;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<SpringJoint>().connectedBody = thermo.GetComponent<Rigidbody>();
        gameObject.GetComponent<CableComponent>().endPoint = thermo.transform;

    }

    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<SpringJoint>().connectedBody = thermo.GetComponent<Rigidbody>();
        gameObject.GetComponent<CableComponent>().endPoint = thermo.transform;
    }
}
