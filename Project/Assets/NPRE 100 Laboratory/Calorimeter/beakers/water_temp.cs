using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class water_temp : MonoBehaviour
{
    public float curr_temp;
    public float amb_temp;
    // Start is called before the first frame update
    void Start()
    {
        curr_temp = amb_temp; 
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<own_temp>().temp = curr_temp;
    }
}
