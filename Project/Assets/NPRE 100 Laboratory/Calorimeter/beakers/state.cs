using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class state : MonoBehaviour
{
    public int stat;
    public GameObject panel;
    // Start is called before the first frame update
    void Start()
    {

        stat = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(stat == 0)
        {
            panel.SetActive(false);
        }
        if (stat == 1)
        {
            panel.SetActive(true);
        }
    }
}