using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temp_thermo : MonoBehaviour
{
    public float temp;
    public float amb;
    public GameObject water;
    // Start is called before the first frame update
    void Start()
    {
        if(water == null)
        {
            temp = amb;
        }
        else
        {
            temp = water.GetComponent<water_temp>().curr_temp;
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        if (water == null)
        {
            temp = amb;
        }
        else
        {
            temp = water.GetComponent<water_temp>().curr_temp;
        }

    }
}
