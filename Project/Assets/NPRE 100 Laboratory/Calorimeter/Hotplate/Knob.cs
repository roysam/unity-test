using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Knob : MonoBehaviour
{
    private bool state;
    public GameObject heatplate;
    public GameObject knob;
    // Start is called before the first frame update
    void Start()
    {
        state = false;
        knob.transform.localPosition = new Vector3(-0.103065f, -0.01900451f, -0.05472311f);
        knob.transform.localEulerAngles = new Vector3(129.752f, -8.166016f, -6.296021f);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseDown()
    {
        Debug.Log("knob");
        if(state == false)
        {
            knob.transform.localPosition = new Vector3(0.01416016f, -0.04548518f, -0.07652728f);
            knob.transform.localEulerAngles = new Vector3(185.535f, -94.57599f, -50.314f);
            heatplate.GetComponent<Temp>().temp = 100;
            state = true;

        }
        else
        {
            knob.transform.localPosition = new Vector3(-0.103065f, -0.01900451f, -0.05472311f);
            knob.transform.localEulerAngles = new Vector3(129.752f, -8.166016f, -6.296021f);
            heatplate.GetComponent<Temp>().temp = 0;
            state = false;

        }
        
    }
}
