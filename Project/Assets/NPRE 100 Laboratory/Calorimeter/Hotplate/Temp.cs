using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temp : MonoBehaviour
{
    public int temp;
    private float tem2;
    public GameObject display;
    public int mode;
    // Start is called before the first frame update
    void Start()
    {
        mode = 0;

    }

    // Update is called once per frame
    void Update()
    {
        if (temp != -1)
        {
            if (mode == 0)
            {
                display.GetComponent<TMPro.TextMeshProUGUI>().text = System.Environment.NewLine + temp.ToString() + " C";

            }
            if (mode == 1)
            {
                tem2 = 273 + temp;

                display.GetComponent<TMPro.TextMeshProUGUI>().text = System.Environment.NewLine + tem2.ToString() + " K";

            }
            if (mode == 2)
            {
                tem2 = (temp * (9 / 5)) + 32;
                display.GetComponent<TMPro.TextMeshProUGUI>().text = System.Environment.NewLine + tem2.ToString() + " F";

            }
        }
        if(temp == -1)
        {
            display.GetComponent<TMPro.TextMeshProUGUI>().text = System.Environment.NewLine+"";
        }

    }
}
