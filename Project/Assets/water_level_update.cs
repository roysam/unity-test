using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class water_level_update : MonoBehaviour
{
    public GameObject inner_cup;
    public float init_mass;
    // Start is called before the first frame update
    void Start()
    {
        init_mass = inner_cup.GetComponent<Rigidbody>().mass;
    }

    // Update is called once per frame
    void Update()
    {
        if (inner_cup.GetComponent<Rigidbody>().mass == init_mass)
        {
            this.transform.localScale = new Vector3(0f, 0f, 0f);
        }
        else
        {
            float z = (inner_cup.GetComponent<Rigidbody>().mass/inner_cup.GetComponent<water_level_cup>().max_mass);
            this.transform.localScale = new Vector3(1f,1f,z);
        }
    }
}
