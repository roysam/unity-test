using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class position : MonoBehaviour
{
    public GameObject obj;
    private Vector3 dif;
    // Start is called before the first frame update
    void Start()
    {
        dif = transform.localPosition - obj.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = obj.transform.localPosition + dif;
    }
}
