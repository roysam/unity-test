using System.Collections.Generic;
using UnityEngine;

public class RealTimeGraph : MonoBehaviour
{
    public GameObject objectToGraph;    // The object whose variable will be graphed
    public float timeInterval = 10f;   // The time interval between graph points
    public int maxPoints = 100;         // The maximum number of points to display on the graph

    private float lastUpdateTime;       // The time of the last graph update
    private List<float> variableValues; // A list of the variable's values over time
    private LineRenderer lineRenderer;  // The LineRenderer component to draw the graph

    void Start()
    {
        variableValues = new List<float>();
        lineRenderer = gameObject.GetComponent<LineRenderer>();
    }

    void Update()
    {
        // Add the current variable value to the list if it's time for an update
        if (Time.time - lastUpdateTime >= timeInterval)
        {
            lastUpdateTime = Time.time;
            variableValues.Add(objectToGraph.GetComponent<own_temp>().temp);
        }

        // Remove the oldest point if the maximum number of points has been reached
        if (variableValues.Count > maxPoints)
        {
            variableValues.RemoveAt(0);
        }

        // Update the graph based on the variable values
        lineRenderer.positionCount = variableValues.Count;
        for (int i = 0; i < variableValues.Count; i++)
        {
            Vector3 pointPosition = new Vector3(i * timeInterval,  0, -0.04f*variableValues[i]);
            lineRenderer.SetPosition(i, transform.TransformPoint(pointPosition));
        }
    }
}