using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tem_thermocouple : MonoBehaviour
{
    public float temp;
    public float amb;
    public GameObject water;
    public GameObject send_sig_sensor;
    // Start is called before the first frame update
    void Start()
    {
        if (water == null)
        {
            temp = amb;
        }
        else
        {
            temp = water.GetComponent<water_temp>().curr_temp;
        }
        send_sig_sensor.GetComponent<temp_sensor>().Tempratur = temp;

    }

    // Update is called once per frame
    void Update()
    {
        if (water == null)
        {
            temp = amb;
        }
        else
        {
            temp = water.GetComponent<water_temp>().curr_temp;

        }
        send_sig_sensor.GetComponent<temp_sensor>().Tempratur = temp;

    }
}
