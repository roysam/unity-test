using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class local_tran : MonoBehaviour
{
    private Vector3 local;
    private int state;
    private float counter;
    public GameObject hotplate;
    public int amb;
    private void Start()
    {
        counter = -1;
        amb = 25;
    }
    void Update()
    {
        if (counter <= 180 && counter >= 0)
        {
            if (state == 1)
            {
                Vector3 position = gameObject.GetComponent<Renderer>().bounds.center;
                gameObject.transform.RotateAround(position, new Vector3(0f, 1f, -1f), -0.2f);
                counter += 0.2f;
            }
            if (state == 2)
            {
                Vector3 position = gameObject.GetComponent<Renderer>().bounds.center;
                gameObject.transform.RotateAround(position, new Vector3(0f, 1f, -1f), 0.2f);
                counter -= 0.2f;
            }
        }
        if(counter > 180)
        {
            if (state == 2)
            {
                Vector3 position = gameObject.GetComponent<Renderer>().bounds.center;
                gameObject.transform.RotateAround(position, new Vector3(0f, 1f, -1f), 0.2f);
                counter -= 0.2f;
            }
        }
        if(counter < 0)
        {
            if (state == 1)
            {
                Vector3 position = gameObject.GetComponent<Renderer>().bounds.center;
                gameObject.transform.RotateAround(position, new Vector3(0f, 1f, -1f), -0.2f);
                counter += 0.2f;
            }
        }

        if (counter > 0)
        {
            hotplate.GetComponent<Temp>().temp = (int)(amb + counter + 0.2);
        }
        else
        {
            hotplate.GetComponent<Temp>().temp = -1;
        }
        
    }
    private void OnMouseDown()
    {
        state = 1;
    }
    private void OnMouseUp()
    {
        state = 0;
    }
    public void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(1))
        {
            state = 2;
        }
        if (Input.GetMouseButtonUp(1))
        {
            state = 0;
        }
    }
}
