using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public static class Extensions
{
    public static T[] Append<T>(this T[] array, T item)
    {
        if (array == null)
        {
            return new T[] { item };
        }
        T[] result = new T[array.Length + 1];
        array.CopyTo(result, 0);
        result[array.Length] = item;
        return result;
    }
    public static float Gauss_noise()
    {
        System.Random rand = new System.Random(); //reuse this if you are generating many
        float u1 = Convert.ToSingle(1.0 - (rand.NextDouble())); //uniform(0,1] random doubles
        float u2 = Convert.ToSingle(1.0 - rand.NextDouble());
        float randStdNormal = Convert.ToSingle(Math.Sqrt(-2.0 * Math.Log(u1)) *
                     Math.Sin(2.0 * Math.PI * u2)); //random normal(0,1)
        float randNormal = randStdNormal;
        return randNormal;
    }
}

public class auto_place : MonoBehaviour
{

    public GameObject placer;
    public GameObject final_pos;
    private float X_min;
    private float X_max;
    private float Z_min;
    private float Z_max;
    private bool is_up;
    private float mass_obj;
    private bool state_up;
    private bool hasdisplay;
    private float y;
    public bool mouse_down;
    float temp2 = 0f;
    // Start is called before the first frame update
    void Start()
    {
        y = transform.position.y;
        BoxCollider col = placer.GetComponent<BoxCollider>();
        var trans = col.transform;
        var min = col.center - col.size * 0.5f;
        var max = col.center + col.size * 0.5f;
        var P000 = trans.TransformPoint(new Vector3(min.x, min.y, min.z));
        var P001 = trans.TransformPoint(new Vector3(max.x, max.y, max.z));
        X_min = P000.x < P001.x ? P000.x : P001.x;
        X_max = P000.x < P001.x ? P001.x : P000.x ;
        Z_min = P000.z < P001.z ? P000.z : P001.z;
        Z_max = P000.z < P001.z ? P001.z : P000.z;
        Rigidbody rb_obj = GetComponent<Rigidbody>();
        mass_obj = rb_obj.mass;
        state_up = false;
        hasdisplay = placer.GetComponent<has_display>().hasdisplay;




    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<is_up>().state == 0 || (gameObject.GetComponent<is_up>().state == 1 && state_up == true))
        {
            Rigidbody rb_obj = GetComponent<Rigidbody>();
            mass_obj = rb_obj.mass;
            float temp = 0f;
            is_up = (transform.position.x < X_max && transform.position.x > X_min) && (transform.position.z < Z_max && transform.position.z > Z_min);
            if (is_up)
            {
                //transform.position = new Vector3(transform.position.x, final_pos.transform.position.y, transform.position.z);
                //Debug.Log(transform.position);
                if (state_up == false && hasdisplay)
                {
                    placer.GetComponent<Mass>().masses = placer.GetComponent<Mass>().masses.Append(mass_obj);
                    state_up = true;
                    temp2 = mass_obj;
                    temp = placer.GetComponent<Mass>().masses[0];
                    placer.GetComponent<nothing_up>().up += 1;
                }



            }
            if (is_up && mouse_down == false)
            {
                transform.position = new Vector3(transform.position.x, final_pos.transform.position.y, transform.position.z);
            }
            if (is_up && mouse_down == false && hasdisplay && (mass_obj != temp2))
            {
                placer.GetComponent<Mass>().masses[0] = temp + mass_obj - temp2;
            }
        }
    }

    public void OnMouseDrag()
    {
        if (gameObject.GetComponent<is_up>().state == 0 || (gameObject.GetComponent<is_up>().state == 1 && state_up == true))
        {
            mouse_down = true;
            if (is_up)
            {
                final_pos.SetActive(true);
            }
            if (!is_up)
            {
                final_pos.SetActive(false);
                if (state_up == true && hasdisplay)
                {
                    placer.GetComponent<Mass>().masses = placer.GetComponent<Mass>().masses.Append(-1 * mass_obj);
                    state_up = false;
                    placer.GetComponent<nothing_up>().up -= 1;
                }
            }
        }


    }

    private void OnMouseUp()
    {
        if (gameObject.GetComponent<is_up>().state == 0 || (gameObject.GetComponent<is_up>().state == 1 && state_up == true))
        {
            mouse_down = false;
            final_pos.SetActive(false);
            if (is_up)
            {
                transform.position = final_pos.transform.position;
                //Debug.Log(transform.position);
                if (state_up == false && hasdisplay)
                {
                    //placer.GetComponent<Mass>().mass_meter += mass_obj;
                    state_up = true;
                    gameObject.GetComponent<is_up>().state = 1;
                }

            }

            if (!is_up)
            {
                state_up = false;
                transform.position = new Vector3(transform.position.x, y, transform.position.z);
                gameObject.GetComponent<is_up>().state = 0;

            }
        }
    }
}
