using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class water_level : MonoBehaviour
{
    public GameObject beaker;
    private float level;
    private float mass;
    private float init_mass;
    public bool is_pouring;
    private float r = 1.16f;
    
    
    // Start is called before the first frame update
    void Start()
    {
        this.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
        is_pouring = false;
        init_mass = beaker.GetComponent<Rigidbody>().mass;
     }

    // Update is called once per frame
    void Update()
    {
        if(this.transform.localScale.y < 1.7f && is_pouring)
        {
            if (this.transform.localScale.x == 0.0f)
            {
                this.transform.localScale = new Vector3(r, 0.0f, r);
            }
            this.transform.localScale = this.transform.localScale + new Vector3(0f, .01f, 0f) * Time.deltaTime;
            mass = (this.transform.localScale.y/1.7f) * 0.5f;
            gameObject.GetComponent<own_temp>().temp = (gameObject.GetComponent<own_temp>().temp * gameObject.GetComponent<Rigidbody>().mass + mass * 26) / (mass + gameObject.GetComponent<Rigidbody>().mass);
            beaker.GetComponent<Rigidbody>().mass = init_mass + mass;
        }
        if(beaker.GetComponent<Rigidbody>().mass >= init_mass)
        {
            if(beaker.GetComponent<Rigidbody>().mass == init_mass)
            {
                this.transform.localScale = new Vector3(0f, 0f, 0f);
            }
            else
            {
                this.transform.localScale = new Vector3(r, (beaker.GetComponent<Rigidbody>().mass - init_mass) * 1.7f / 0.5f, r);
            }

        }
            is_pouring = false;
        gameObject.GetComponent<Rigidbody>().mass = beaker.GetComponent<Rigidbody>().mass - init_mass + 0.000001f;
    }
}
