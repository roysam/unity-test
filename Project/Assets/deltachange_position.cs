using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deltachange_position : MonoBehaviour
{
    public bool change;
    public Vector3 delta1;
    public Vector3 delta2;
    public Vector3 change_;
    public int i;
    // Start is called before the first frame update
    void Start()
    {
        i = 0;   
    }

    // Update is called once per frame
    void Update()
    {
        if (i <= 10)
        {
            change = true;
        }

        if (i%2 == 0)
        {
            delta1 = transform.position;
        }
        else
        {
            delta2 = transform.position;
        }
        change_ = delta1 - delta2;
        if(change_ == new Vector3(0, 0, 0) && i>=10)
        {
            change = false;
        }
        else if(change_ != new Vector3(0, 0, 0) && i >= 10 && i >5)
        {
            change = true;
            i = 0;
        }
        i = i+1;
        if (i > 10000)
        {
            i = 0;
        }
    }
}
