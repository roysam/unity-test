using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class series_generator : MonoBehaviour
{
    public List<Vector2> temperature_data;
    public GameObject thermocouple;
    float timer;
    float temperature;
    public float interval =10000f;
    float delta_time;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        delta_time += Time.deltaTime;
        if (delta_time >= interval)
        {
            temperature = thermocouple.GetComponent<own_temp>().temp;
            temperature_data.Add(new Vector2(timer, temperature));
            delta_time = 0f;
        }
    }
}
