using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class box_collider_remover : MonoBehaviour
{
    public GameObject obj;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if(gameObject.GetComponent<Auto_place_dynamic>().state_up == true)
        {
            obj.GetComponent<BoxCollider>().enabled = false;
        }
        else
        {
            obj.GetComponent<BoxCollider>().enabled = true;
        }
    }
}
