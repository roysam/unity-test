using System.Collections;

using System.Collections.Generic;

using UnityEngine;
public class DragObject : MonoBehaviour

{

    private Vector3 mOffset;
    private float mZCoord;
    public GameObject place;
    public float xcor_min;
    public float xcor_max;
    public float zcor_min;
    public float zcor_max;
    public float y;


    private void Start()
    {
        BoxCollider col = place.GetComponent<BoxCollider>();
        var trans = col.transform;
        var min = col.center - col.size * 0.5f;
        var max = col.center + col.size * 0.5f;
        var P000 = trans.TransformPoint(new Vector3(min.x, min.y, min.z));
        var P001 = trans.TransformPoint(new Vector3(max.x, max.y, max.z));
        xcor_min = P000.x < P001.x ? P000.x : P001.x;
        xcor_max = P000.x < P001.x ? P001.x : P000.x;
        zcor_min = P000.z < P001.z ? P000.z : P001.z;
        zcor_max = P000.z < P001.z ? P001.z : P000.z;
        y = P000.y < P001.y ? P001.y : P000.y;
    }

    private void Update()
    {
        if (transform.position.y < y)
        {
            transform.position = new Vector3(transform.position.x, y, transform.position.z);
        }
        if (transform.position.x < xcor_min)
        {
            transform.position = new Vector3(xcor_min, y, transform.position.z);
        }
        if (transform.position.x > xcor_max)
        {
            transform.position = new Vector3(xcor_max, y, transform.position.z);
        }
        if (transform.position.z < zcor_min)
        {
            transform.position = new Vector3(transform.position.x, y, zcor_min);
        }
        if (transform.position.z > zcor_max)
        {
            transform.position = new Vector3(transform.position.x, y, zcor_max);
        }
        if (transform.position.y < y)
        {
            transform.position = new Vector3(transform.position.x, y, transform.position.z);
        }

        Rigidbody rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(0f, rb.velocity.y, 0f);


    }

    void OnMouseDown()
    {
        mZCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;

        // Store offset = gameobject world pos - mouse world pos

        mOffset = gameObject.transform.position - GetMouseAsWorldPoint();

    }



    private Vector3 GetMouseAsWorldPoint()

    {

        // Pixel coordinates of mouse (x,y)

        Vector3 mousePoint = Input.mousePosition;



        // z coordinate of game object on screen

        mousePoint.z = mZCoord;



        // Convert it to world points

        return Camera.main.ScreenToWorldPoint(mousePoint);

    }
    void OnMouseDrag()
    {

        transform.position = GetMouseAsWorldPoint() + mOffset;


    }

}
