using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boil_mass_loss : MonoBehaviour
{
    public float mass_rate;
    public GameObject mass;
    private float init_mass;
    public float diff;
    // Start is called before the first frame update
    void Start()
    {
        init_mass = mass.GetComponent<Rigidbody>().mass;
    }

    // Update is called once per frame
    void Update()
    {
        diff = mass.GetComponent<Rigidbody>().mass - init_mass;
        float temp = gameObject.GetComponent<conduction>().t1;
        if(temp >= 100 && mass.GetComponent<Rigidbody>().mass > init_mass+0.00001)
        {
                mass_rate = 0.01f*(gameObject.GetComponent<conduction>().heat_ener/2260);
                mass.GetComponent<Rigidbody>().mass -= Mathf.Abs(mass_rate);
           
        }
        if (mass.GetComponent<Rigidbody>().mass < init_mass)
        {
            mass.GetComponent<Rigidbody>().mass = init_mass;
        }

    }
}
