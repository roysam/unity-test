using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mass : MonoBehaviour
{
    public float mass_meter;
    public float[] masses = {0f};
    public GameObject display;
    public float level = 0f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        mass_meter = 0;
        for (int i = 0; i < masses.Length; i++)
        {
            mass_meter += masses[i];
        }
        if(mass_meter == 0f && gameObject.GetComponent<nothing_up>().up == 0)
        {
            float[] temp = { 0f };
            masses = temp;
        }
        if((mass_meter-level) >= 0f)
        {
            display.GetComponent<TMPro.TextMeshProUGUI>().text = System.Environment.NewLine + System.Math.Round(((mass_meter - level)* 1000), 2).ToString();
        }
        else
        {
            display.GetComponent<TMPro.TextMeshProUGUI>().text = System.Environment.NewLine +(0.00).ToString();
        }


    }
}