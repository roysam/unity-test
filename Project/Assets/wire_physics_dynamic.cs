using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wire_physics_dynamic : MonoBehaviour
{
    public GameObject wire_builder;
    private WireController wire;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(gameObject.GetComponent<deltachange_position>().change == true)
        {
            wire = wire_builder.GetComponent<WireController>();
            wire.usePhysics = true; 
        }
        else
        {
            wire = wire_builder.GetComponent<WireController>();
            wire.usePhysics = false ;
        }
    }
}
