using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onsqueeze : MonoBehaviour
{
    public GameObject waterpour;
    public GameObject dropper;
    private int state = 0;
    private Vector3 rot;
    // Start is called before the first frame update
    void Start()
    {
        waterpour.SetActive(false);
        rot = gameObject.transform.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(1))
        {
            waterpour.SetActive(false);
            gameObject.transform.eulerAngles = rot;
        }
    }
    private void OnMouseOver()
    {

        if (Input.GetMouseButtonDown(1))
        {
            waterpour.SetActive(true);
            gameObject.transform.Rotate(new Vector3(0, 0, 60));
        }
    }

}
