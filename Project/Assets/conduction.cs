using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class conduction : MonoBehaviour
{
    public bool on_flame;
    public GameObject hotplate;
    public int specific_heat;
    public float t1;
    public float t2;
    public float ambient_t;
    public float mass;
    public float heat_ener;
    public float thermal_con;
    public float rating;
    private float latent_heat;
    public GameObject time;
    private float t;
    // Start is called before the first frame update
    void Start()
    {
        rating = 400;
        t2 = GetComponent<water_temp>().amb_temp;
        ambient_t = t2;
        t1 = GetComponent<water_temp>().curr_temp;
        thermal_con = 0.2f;
        mass = GetComponent<Rigidbody>().mass;
        specific_heat = 4187;
       
    }

    // Update is called once per frame
    void Update()
    {
        t = time.GetComponent<global_time>().global_times;
        if (on_flame)
        {
            if(hotplate.GetComponent<Temp>().temp == -1)
            {
                t2 = GetComponent<water_temp>().amb_temp;
            }
            else
            {
                t2 = hotplate.GetComponent<Temp>().temp;
            }
            if (t1 <= t2)
            {
                heat_ener = (rating*Time.deltaTime * t - ((t1 - ambient_t) * thermal_con * Time.deltaTime * t));
                t1 = t1 + (heat_ener / (specific_heat * mass));
                //old code
                //heat_ener = (t2 - t1) * thermal_con * Time.deltaTime*t;
                //t1 = t1 + (heat_ener / (specific_heat*mass));
                GetComponent<water_temp>().curr_temp = t1;
                //Debug.Log(t1);
            }
            else
            {
                t1 = t1 + 0.001f * Extensions.Gauss_noise();
            }


        }
        if(!on_flame)
        {
            heat_ener = (t1 - ambient_t) * thermal_con * Time.deltaTime * t;
            t1 = t1 - (heat_ener / (specific_heat * mass));
            //Debug.Log(t1);
            GetComponent<water_temp>().curr_temp = t1;
        }

        
    }
}
