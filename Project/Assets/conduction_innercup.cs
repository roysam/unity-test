using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class conduction_innercup : MonoBehaviour
{
    public GameObject ambient;
    public GameObject inner_cup;
    public float conductivity;
    private float t1;
    private float t2;
    public float cylinderRadius = 0.0035f;
    public float cylinderLength = 0.0055f;
    public float heatTransferCoefficient;
    private float surfaceArea;
    public float heat_ener;
    public GameObject time;
    public int specific_heat;
    // Start is called before the first frame update
    void Start()
    {
        specific_heat = 4187;
        surfaceArea = 2 * Mathf.PI * cylinderRadius * cylinderLength + 2 * Mathf.PI * Mathf.Pow(cylinderRadius, 2);
        heatTransferCoefficient = 400;
    }

    // Update is called once per frame
    void Update()
    {
        if (inner_cup.GetComponent<surround_medium>().surround == ambient)
        {
            t1 = gameObject.GetComponent<own_temp>().temp;
            t2 = ambient.GetComponent<own_temp>().temp;
            heat_ener = heatTransferCoefficient * surfaceArea * (t1 - t2)* Time.deltaTime;
            t1 = t1 - (heat_ener / (specific_heat * gameObject.GetComponent<Rigidbody>().mass));
            //old code
            //heat_ener = (t2 - t1) * thermal_con * Time.deltaTime*t;
            //t1 = t1 + (heat_ener / (specific_heat*mass));
            gameObject.GetComponent<own_temp>().temp = t1;
        }
    }
}