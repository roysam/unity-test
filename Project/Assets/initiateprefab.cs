using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class initiateprefab : MonoBehaviour
{
    public GameObject emptygraphprefab;
    public WMG_Axis_Graph graph;
    public List<Vector2> series1Data;
    public WMG_Series series1;
    public Color pointcolor;
    public Color linecolor;
    float timer = 0f;
    int oldsec;
    public float linescale = 0f;
    GameObject graphgo;
    float xscale = 0.013461f;
    float yscale = 0.04504973f;
    float zscale = 0.027729f;
    int counter=0;

    // Start is called before the first frame update
    void Start()
    {
        graphgo = GameObject.Instantiate(emptygraphprefab);
        graphgo.transform.SetParent(this.transform, false);
        graph = graphgo.GetComponent<WMG_Axis_Graph>();
        series1 = graph.addSeries();
        series1.pointValues.SetList(series1Data);
        series1.pointValues.SetList(series1Data);
        graphgo.transform.localScale = new Vector3(xscale, yscale, zscale);
    }

    void Update()
    {

        timer += Time.deltaTime;
        int seconds = (int)timer % 60 + ((int)timer /60)*60;
        series1Data = gameObject.GetComponents<series_generator>()[0].temperature_data;
        series1.pointValues.SetList(series1Data);
        if (seconds > oldsec)
        {
            counter += 1;
            if(counter ==   10)
            {
                graphgo.transform.localScale = new Vector3(0, 0, 0);
                Destroy(graphgo);
                graphgo = GameObject.Instantiate(emptygraphprefab);
                graphgo.transform.SetParent(this.transform, false);
                graph = graphgo.GetComponent<WMG_Axis_Graph>();
                series1 = graph.addSeries();
                series1.pointValues.SetList(series1Data);
                series1.pointValues.SetList(series1Data);
                series1.lineScale = linescale;
                series1.lineColor = linecolor;
                series1.pointColor = pointcolor;
                counter = 0;
                graph.xAxis.AxisMaxValue = seconds + 20;
                graphgo.transform.localScale = new Vector3(xscale, yscale, zscale);
            }

        }
        oldsec = seconds;
    }
}
