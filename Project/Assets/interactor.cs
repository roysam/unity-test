using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class interactor : MonoBehaviour
{
    public LayerMask iteractable_layer_mask;
    Interactable interactable;
    public Sprite interactimage;
    public Sprite defaulticon;
    public Sprite defaultinteracticon;
    // Start is called before the first frame update
    void Start()
    {

    }


    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 2, iteractable_layer_mask))
        {

            if (hit.collider.GetComponent<Interactable>() != false)
            {
                if (interactable == null || interactable.ID != hit.collider.GetComponent<Interactable>().ID)
                {
                    interactable = hit.collider.GetComponent<Interactable>();

                    Debug.Log("new");
                }
                if (interactable.interacticon != null)
                {
                    interactimage = interactable.interacticon;

                }
                else
                {
                    interactimage = defaultinteracticon;
                }
                //this portion is for various interactions possible
                if (Input.GetKeyDown(KeyCode.E))
                {
                    //for auto hold objects
                    if (interactable.Tag == "auto hold")
                    {
                        Debug.Log("Tag");
                        if (interactable.c % 2 == 0)
                        {
                            interactable.curr.transform.position = interactable.fin_posObj;
                            interactable.curr.transform.eulerAngles = interactable.fin_angObj;
                        }
                        if (interactable.c % 2 != 0)
                        {
                            interactable.curr.transform.position = interactable.posObj;
                            interactable.curr.transform.eulerAngles = interactable.angObj;
                        }
                        interactable.c += 1;
                    }
                    //for auto hold objects
                    if (interactable.Tag == "Panel")
                    {

                            //Debug.Log("hola2");
                            interactable.GetComponent<state>().stat = 1;



                    }
                }
                if (Input.GetKeyDown(KeyCode.X))
                {
                    if (interactable.Tag == "Panel")
                    {
                            Debug.Log("hola3");
                            interactable.GetComponent<state>().stat = 0;



                    }
                    //holding and draging
                }
            }
        }
        else
        {
            if (interactimage != defaulticon)
            {
                interactimage = defaulticon;
            }
        }
    }
}
