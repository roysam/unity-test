using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vapour_arise : MonoBehaviour
{
    public GameObject vapour;
    public GameObject bubble;
    public float boiling_temp;
    // Start is called before the first frame update
    void Start()
    {
        vapour.SetActive(false);
        bubble.SetActive(false);
        boiling_temp = 99.5f + 0.4f * Extensions.Gauss_noise();
    }

    // Update is called once per frame
    void Update()
    {

        if(gameObject.GetComponent<boil_mass_loss>().diff > 0.00001)
        {

            if (gameObject.GetComponent<water_temp>().curr_temp >= boiling_temp)
            {
                vapour.SetActive(true);
                vapour.GetComponent<ParticleSystem>().Play();
                bubble.SetActive(true);
                bubble.GetComponent<ParticleSystem>().Play();
                bubble.GetComponent<ParticleSystem>().playbackSpeed = 0.3f;

            }
            else
            {
                vapour.SetActive(false);
                bubble.SetActive(false);
            }

        }
        else
        {
            vapour.SetActive(false);
            bubble.SetActive(false);
        }


    }
}
