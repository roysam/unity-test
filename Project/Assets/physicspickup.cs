using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class physicspickup : MonoBehaviour
{
    [SerializeField] private LayerMask pickupMask;
    [SerializeField] private Camera playercamera;
    [SerializeField] private Transform pickuptarget;
    [Space]
    [SerializeField] private float pickupRange;
    [SerializeField] Rigidbody currentobject;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
            if(currentobject)
            {
                currentobject.useGravity = true;
                currentobject = null;
                return;
            }
        {
            Ray CameraRay = playercamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
            if (Physics.Raycast(CameraRay, out RaycastHit HitInfo, pickupRange, pickupMask))
            {
                currentobject = HitInfo.rigidbody;
                currentobject.useGravity = false;
                
            }
        }
        
    }
    private void FixedUpdate()
    {
        if(currentobject)
        {
            Vector3 directionpoint = pickuptarget.position - currentobject.position;
            float distancetopoint = directionpoint.magnitude;
            currentobject.velocity = directionpoint * 12f * distancetopoint;
            

        }
    }
}
