using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class update_temp : MonoBehaviour
{
    public GameObject temp;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<own_temp>().temp = temp.GetComponent<own_temp>().temp;
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<own_temp>().temp = temp.GetComponent<own_temp>().temp;
    }
}
