using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    public Animator animator;
    public void togglebool(string boolname)
    {
        animator.SetBool(boolname, !animator.GetBool(boolname));
    }
}
