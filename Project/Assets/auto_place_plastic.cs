using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class auto_place_plastic : MonoBehaviour
{
    public GameObject placer;
    public GameObject final_pos;
    public float X_min;
    public float X_max;
    public float Z_min;
    public float Z_max;
    private bool is_up;
    private float mass_obj;
    private bool state_up;
    private float y;
    public bool mouse_down;
    private bool is_placed;
    // Start is called before the first frame update
    void Start()
    {
        y = gameObject.transform.position.y;
        BoxCollider col = placer.GetComponent<BoxCollider>();
        var trans = col.transform;
        var min = col.center - col.size * 0.5f;
        var max = col.center + col.size * 0.5f;
        var P000 = trans.TransformPoint(new Vector3(min.x, min.y, min.z));
        var P001 = trans.TransformPoint(new Vector3(max.x, max.y, max.z));
        X_min = P000.x < P001.x ? P000.x : P001.x;
        X_max = P000.x < P001.x ? P001.x : P000.x;
        Z_min = P000.z < P001.z ? P000.z : P001.z;
        Z_max = P000.z < P001.z ? P001.z : P000.z;
        Rigidbody rb_obj = GetComponent<Rigidbody>();
        mass_obj = rb_obj.mass;
        state_up = false;



    }

    // Update is called once per frame
    void Update()
    {
        BoxCollider col = placer.GetComponent<BoxCollider>();
        var trans = col.transform;
        var min = col.center - col.size * 0.5f;
        var max = col.center + col.size * 0.5f;
        var P000 = trans.TransformPoint(new Vector3(min.x, min.y, min.z));
        var P001 = trans.TransformPoint(new Vector3(max.x, max.y, max.z));
        X_min = P000.x < P001.x ? P000.x : P001.x;
        X_max = P000.x < P001.x ? P001.x : P000.x;
        Z_min = P000.z < P001.z ? P000.z : P001.z;
        Z_max = P000.z < P001.z ? P001.z : P000.z;
        is_up = (transform.position.x < X_max && transform.position.x > X_min) && (transform.position.z < Z_max && transform.position.z > Z_min);
        if (is_up && mouse_down == false)
        {
            transform.position = new Vector3(transform.position.x, final_pos.transform.position.y, transform.position.z);
        }
        if(is_placed)
        {
            //transform.position = new Vector3(transform.position.x, final_pos.transform.position.y, transform.position.z);
            Debug.Log("metal");
            transform.position = final_pos.transform.position;

        }


    }

    public void OnMouseDrag()
    {
        mouse_down = true;
        if (is_up)
        {
            final_pos.SetActive(true);
        }
        if (!is_up)
        {
            final_pos.SetActive(false);

        }


    }

    private void OnMouseUp()
    {
        mouse_down = false;
        final_pos.SetActive(false);
        if (is_up)
        {
            transform.position = final_pos.transform.position;
            Debug.Log(transform.position);
            gameObject.GetComponent<is_up>().state += 1;
            is_placed = true;

        }

        if (!is_up)
        {
            transform.position = new Vector3(transform.position.x, y, transform.position.z);

        }
    }
    private void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(1))
        {
            is_placed = false;
            //Debug.Log("not_placed");
        }
        //Debug.Log("is_touching");
    }
}
