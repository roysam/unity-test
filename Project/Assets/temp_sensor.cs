using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class temp_sensor : MonoBehaviour
{
    public bool is_connected;
    public float Tempratur;
    public GameObject display;
    // Start is called before the first frame update
    void Start()
    {
        display.GetComponent<TMPro.TextMeshProUGUI>().text = "Not connected";

    }

    // Update is called once per frame
    void Update()
    {
        if (!is_connected)
        {
            display.GetComponent<TMPro.TextMeshProUGUI>().text = "Not connected";
        }
        else
        {
            display.GetComponent<TMPro.TextMeshProUGUI>().text = Tempratur.ToString("0.##");
        }

    }
}
