using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class on_mouse0ver : MonoBehaviour
{
    public GameObject obj;
    private Outline myoutline;
    // Start is called before the first frame update
    void Start()
    {
        myoutline = obj.GetComponent<Outline>();
        myoutline.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        //myoutline.enabled =false;
    }
    private void OnMouseOver()
    {
        myoutline.enabled = true;
    }
    private void OnMouseExit()
    {
        myoutline.enabled = false;
    }
}
