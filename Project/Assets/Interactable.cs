using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Interactable : MonoBehaviour
{
    public UnityEvent onInteract;
    public int ID;
    public Sprite interacticon;
    public GameObject curr;
    public GameObject fin;
    public int c = 0;
    public Vector3 posObj;
    public Vector3 angObj;
    public Vector3 fin_posObj;
    public Vector3 fin_angObj;
    public string Tag; 
    // Start is called before the first frame update
    void Start()
    {

        ID = Random.Range(0, 99999);
        posObj = curr.transform.position;
        angObj = curr.transform.eulerAngles;
        fin_posObj = fin.transform.position;
        fin_angObj = fin.transform.eulerAngles;
        Tag = curr.tag;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
