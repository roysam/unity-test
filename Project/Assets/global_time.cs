using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class global_time : MonoBehaviour
{
    public float global_times;
    public GameObject time_disp;
    // Start is called before the first frame update
    void Start()
    {
        global_times = 1f;
        time_disp.GetComponent<TMPro.TextMeshProUGUI>().text =  global_times.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        time_disp.GetComponent<TMPro.TextMeshProUGUI>().text = Mathf.Round(global_times).ToString();
    }
}
