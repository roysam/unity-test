using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class on_click_transform_global : MonoBehaviour
{
    private bool state = false;
    public Vector3 pos_final;
    public bool same_pos = false;
    public bool same_ang = false;
    public GameObject obj;
    private Vector3 pos_in;
    public Vector3 ang_final;
    private Vector3 ang_in;
    // Start is called before the first frame update
    void Start()
    {
        pos_in = obj.transform.position;
        ang_in = obj.transform.eulerAngles;
        if (same_pos)
        {
            pos_final = pos_in;
        }
        if (same_ang)
        {
            ang_final = ang_in;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (state)
        {
            obj.transform.position = pos_final;
            obj.transform.eulerAngles = ang_final;
        }
        else
        {
            obj.transform.position = pos_in;
            obj.transform.eulerAngles = ang_in;
        }
    }
    private void OnMouseDown()
    {
        state = state ? false : true;
    }
}
