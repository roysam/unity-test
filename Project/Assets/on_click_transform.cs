using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public static class hasComponent
{
    public static bool HasComponent<T>(this GameObject flag) where T : Component
    {
        return flag.GetComponent<T>() != null;
    }
}

public class on_click_transform : MonoBehaviour
{
    private bool state= false;
    public Vector3 pos_final;
    public bool same_pos = false;
    public bool same_ang= false;
    public GameObject obj;
    private Vector3 pos_in;
    public Vector3 ang_final;
    private Vector3 ang_in;
    // Start is called before the first frame update
    void Start()
    {
        pos_in = obj.transform.localPosition;
        ang_in = obj.transform.localEulerAngles;
        if (same_pos)
        {
            pos_final = pos_in;
        }
        if (same_ang)
        {
            ang_final = ang_in;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (state)
        {
            obj.transform.localPosition= pos_final;
            obj.transform.localEulerAngles = ang_final;
        }
        else
        {
            obj.transform.localPosition = pos_in;
            obj.transform.localEulerAngles = ang_in;
        }
        if (gameObject.HasComponent<state>())
        {
            gameObject.GetComponent<state>().stat = state ? 1 : 0;
        }
    }
    private void OnMouseDown()
    {
     state =    state ? false : true;
    }

}
