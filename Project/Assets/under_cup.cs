using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class under_cup : MonoBehaviour
{
    public GameObject beaker;
    public bool is_up;
    private float X_min, X_max, Z_min, Z_max;
    private GameObject water;
    // Start is called before the first frame update
    void Start()
    {
        is_up = false;
        water = beaker.transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        BoxCollider col = beaker.GetComponent<BoxCollider>();
        var trans = col.transform;
        var min = col.center - col.size * 0.5f;
        var max = col.center + col.size * 0.5f;
        var P000 = trans.TransformPoint(new Vector3(min.x, min.y, min.z));
        var P001 = trans.TransformPoint(new Vector3(max.x, max.y, max.z));
        X_min = P000.x < P001.x ? P000.x : P001.x;
        X_max = P000.x < P001.x ? P001.x : P000.x;
        Z_min = P000.z < P001.z ? P000.z : P001.z;
        Z_max = P000.z < P001.z ? P001.z : P000.z;
        if((transform.position.x < X_max && transform.position.x > X_min) && (transform.position.z < Z_max && transform.position.z > Z_min))
        {
            is_up = true;
            water.GetComponent<water_level>().is_pouring = is_up;
            ParticleSystem ps = GetComponent<ParticleSystem>();
            var coll = ps.collision;
            ps.startLifetime = 0.2f;
            coll.enabled = false;

        }
        else
        {
            is_up = false;
            water.GetComponent<water_level>().is_pouring = is_up;
            ParticleSystem ps = GetComponent<ParticleSystem>();
            var coll = ps.collision;
            coll.enabled = true;
            ps.startLifetime = 1.5f;
        }

        //print(is_up);
    }
}
