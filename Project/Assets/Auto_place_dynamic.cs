using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Auto_place_dynamic : MonoBehaviour
{
        public GameObject placer;
        public GameObject final_pos;
        private float X_min;
        private float X_max;
        private float Z_min;
        private float Z_max;
        public bool is_up;
        public bool state_up;
        private float y;
        public bool mouse_down;
        private BoxCollider col;
        public GameObject ambient;
        public bool is_thermocup;
        // Start is called before the first frame update
        void Start()
        {
            
            final_pos.GetComponent<MeshRenderer>().enabled = false;
            y = transform.position.y;
            col = placer.GetComponent<BoxCollider>();
            var trans = col.transform;
            var min = col.center - col.size * 0.5f;
            var max = col.center + col.size * 0.5f;
            var P000 = trans.TransformPoint(new Vector3(min.x, min.y, min.z));
            var P001 = trans.TransformPoint(new Vector3(max.x, max.y, max.z));
            X_min = P000.x < P001.x ? P000.x : P001.x;
            X_max = P000.x < P001.x ? P001.x : P000.x;
            Z_min = P000.z < P001.z ? P000.z : P001.z;
            Z_max = P000.z < P001.z ? P001.z : P000.z;
            Rigidbody rb_obj = GetComponent<Rigidbody>();
            state_up = false;
            is_up = false;
        if (gameObject.GetComponent<is_up>().state == 0 && is_thermocup)
        {
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, 0f);
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0f);
        }

    }
    private void OnMouseDown()
    {
        mouse_down = true;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 v3 = gameObject.GetComponent<Rigidbody>().velocity;
        v3.x = 0f;
        v3.z = 0f;
        gameObject.GetComponent<Rigidbody>().velocity = v3;

        if(gameObject.GetComponent<is_up>().state == 0 || (gameObject.GetComponent<is_up>().state == 1 && state_up == true))
        {
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            var trans = col.transform;
            var min = col.center - col.size * 0.5f;
            var max = col.center + col.size * 0.5f;
            var P000 = trans.TransformPoint(new Vector3(min.x, min.y, min.z));
            var P001 = trans.TransformPoint(new Vector3(max.x, max.y, max.z));
            X_min = P000.x < P001.x ? P000.x : P001.x;
            X_max = P000.x < P001.x ? P001.x : P000.x;
            Z_min = P000.z < P001.z ? P000.z : P001.z;
            Z_max = P000.z < P001.z ? P001.z : P000.z;


            //state_up = (transform.position )
            is_up = (transform.position.x < X_max && transform.position.x > X_min) && (transform.position.z < Z_max && transform.position.z > Z_min);
            if (is_up)
            {
                //transform.position = new Vector3(transform.position.x, final_pos.transform.position.y, transform.position.z);
                //Debug.Log(transform.position);

            }
            if (is_up && mouse_down == false)
            {
                transform.position = new Vector3(final_pos.transform.position.x, final_pos.transform.position.y, final_pos.transform.position.z);
            }
            if (state_up == true && mouse_down == false)
            {
                transform.position = final_pos.transform.position;
                transform.eulerAngles = final_pos.transform.eulerAngles;
                if (is_thermocup)
                {
                    gameObject.GetComponent<own_temp>().temp = final_pos.GetComponent<own_temp>().temp;
                }


            }

        }


    }

    public void OnMouseDrag()
    {
        if (gameObject.GetComponent<is_up>().state == 0 || (gameObject.GetComponent<is_up>().state == 1 && state_up == true))
        {
            mouse_down = true;
            if (is_up)
            {
                //final_pos.SetActive(true);
                final_pos.GetComponent<MeshRenderer>().enabled = true;
            }
            if (!is_up)
            {
                //final_pos.SetActive(false);
                final_pos.GetComponent<MeshRenderer>().enabled = false;
            }


        }
    }

    private void OnMouseUp()
    {
        if (gameObject.GetComponent<is_up>().state == 0 || (gameObject.GetComponent<is_up>().state == 1 && state_up == true))
        {
            mouse_down = false;
            final_pos.GetComponent<MeshRenderer>().enabled = false;
            //final_pos.SetActive(false);
            if (is_up)
            {
                transform.position = final_pos.transform.position;
                transform.eulerAngles = final_pos.transform.eulerAngles;
                //Debug.Log(transform.position);
                state_up = true;
                if (is_thermocup)
                {
                    gameObject.GetComponent<own_temp>().temp = final_pos.GetComponent<own_temp>().temp;
                }
                gameObject.GetComponent<is_up>().state = 1;
                gameObject.GetComponent<surround_medium>().surround = placer;
            }

            if (!is_up)
            {
                transform.position = new Vector3(transform.position.x, y, transform.position.z);
                state_up = false;
                if (is_thermocup)
                {
                    gameObject.GetComponent<own_temp>().temp = ambient.GetComponent<own_temp>().temp;
                }
                gameObject.GetComponent<is_up>().state = 0;
                gameObject.GetComponent<surround_medium>().surround = ambient;
            }
        }

        if (gameObject.GetComponent<is_up>().state == 0 && is_thermocup)
        {
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, 0f);
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0f);
        }

    }
}
