using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class water_level_cup : MonoBehaviour
{
    public float mass;
    public bool poured;
    public float max_mass;
    public GameObject beaker;
    public GameObject beaker_parent;
    public float init_mass;
    private float r = 0.066f;
    
    // Start is called before the first frame update
    void Start()
    {
        poured = false;
        max_mass = 0.05f;
        gameObject.GetComponent<Rigidbody>().mass = 0.00001f;
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        if (poured && (beaker_parent.GetComponent<tilt_beaker>().water_lost>0f))
        {
            gameObject.GetComponent<Rigidbody>().mass = Math.Min(beaker_parent.GetComponent<tilt_beaker>().water_lost + gameObject.GetComponent<Rigidbody>().mass, max_mass);
            beaker_parent.GetComponent<tilt_beaker>().water_lost = 0f;
            poured = false;
            gameObject.GetComponent<own_temp>().temp = beaker.GetComponent<own_temp>().temp;
        }

    }
}
