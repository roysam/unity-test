using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class auto_place_hotplate : MonoBehaviour
{

    public GameObject placer;
    public GameObject final_pos;
    private float X_min;
    private float X_max;
    private float Z_min;
    private float Z_max;
    private bool is_up;
    private float mass_obj;
    private bool state_up;
    private bool hasdisplay;
    private float y;
    public GameObject water;
    // Start is called before the first frame update
    void Start()
    {
        y = transform.position.y;
        BoxCollider col = placer.GetComponent<BoxCollider>();
        var trans = col.transform;
        var min = col.center - col.size * 0.5f;
        var max = col.center + col.size * 0.5f;
        var P000 = trans.TransformPoint(new Vector3(min.x, min.y, min.z));
        var P001 = trans.TransformPoint(new Vector3(max.x, max.y, max.z));
        X_min = P000.x < P001.x ? P000.x : P001.x;
        X_max = P000.x < P001.x ? P001.x : P000.x;
        Z_min = P000.z < P001.z ? P000.z : P001.z;
        Z_max = P000.z < P001.z ? P001.z : P000.z;
        Rigidbody rb_obj = GetComponent<Rigidbody>();
        mass_obj = rb_obj.mass;
        state_up = false;
        hasdisplay = placer.GetComponent<has_display>().hasdisplay;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<is_up>().state == 0 || (gameObject.GetComponent<is_up>().state == 1 && state_up == true))
        {
            is_up = (transform.position.x < X_max && transform.position.x > X_min) && (transform.position.z < Z_max && transform.position.z > Z_min);
            if (is_up)
            {
                if (water != null)
                {
                    water.GetComponent<conduction>().on_flame = true;
                }
                transform.position = final_pos.transform.position;
                //Debug.Log(transform.position);
                if (state_up == false && hasdisplay)
                {
                    placer.GetComponent<Mass>().mass_meter += mass_obj;
                    state_up = true;
                }
                //gameObject.GetComponent<is_up>().state += 1;

            }
            if (!is_up)
            {
                if (water != null)
                {
                    water.GetComponent<conduction>().on_flame = false;
                }
            }
        }
    }

    public void OnMouseDrag()
    {
        if (gameObject.GetComponent<is_up>().state == 0 || (gameObject.GetComponent<is_up>().state == 1 && state_up == true))
        {
            if (is_up)
            {
                final_pos.SetActive(true);
            }
            if (!is_up)
            {
                final_pos.SetActive(false);
                if (state_up == true && hasdisplay)
                {
                    placer.GetComponent<Mass>().mass_meter -= mass_obj;
                    state_up = false;
                }
            }
        }


    }

    private void OnMouseUp()
    {
        if (gameObject.GetComponent<is_up>().state == 0 || (gameObject.GetComponent<is_up>().state == 1 && state_up == true))
        {
            final_pos.SetActive(false);
            if (is_up)
            {
                transform.position = final_pos.transform.position;
                //Debug.Log(transform.position);
                if (state_up == false && hasdisplay)
                {
                    placer.GetComponent<Mass>().mass_meter += mass_obj;
                    state_up = true;
                    gameObject.GetComponent<is_up>().state = 1;
                }

            }

            if (!is_up)
            {
                transform.position = new Vector3(transform.position.x, y, transform.position.z);
                gameObject.GetComponent<is_up>().state = 0;
                state_up = false;
            }
        }
    }
}
